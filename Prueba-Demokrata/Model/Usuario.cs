﻿using System.ComponentModel.DataAnnotations;

namespace Prueba_Demokrata.Model
{
    public class Usuario
    {
        public int Id { get; set; }
        
        [StringLength(50)]
        public required string PrimerNombre { get; set; }
        
        [StringLength(50)]
        public string? SegundoNombre { get; set; }
        
        [StringLength(50)]
        public required string PrimerApellido { get; set; }
        
        [StringLength(50)]
        public string? SegundoApellido { get; set; }
        public required DateOnly Fecha_Nacimiento { get; set; }
        public required int Sueldo {  get; set; }
        public DateTime Fecha_Creacion { get; set; }
        public DateTime Fecha_Modificacion { get; set; }
       
        
    }
}
