﻿using Microsoft.EntityFrameworkCore;
using Prueba_Demokrata.Model;

namespace Prueba_Demokrata.Context
{
    public class AppDbContext: DbContext
    {
        public AppDbContext(DbContextOptions <AppDbContext> options): base(options)
        {

        }

        public DbSet<Usuario>  Usuarios { get; set; }
    }
}
